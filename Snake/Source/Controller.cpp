#include<SFML\Window.hpp>
#include "Controller.h"
#include<string>

using namespace std;
using namespace GAME;
using namespace sf;

void LocalController::OnKeyDown(sf::Keyboard::Key key)
{
	if (key == Controls[0]) MoveSnakes(-1, 0);
	else if (key == Controls[1]) MoveSnakes(1, 0);
	else if (key == Controls[2]) MoveSnakes(0, -1);
	else if (key == Controls[3]) MoveSnakes(0, 1);
}

void Controller::MoveSnakes(int dx, int dy)
{
	for (int i=0;i<Snakes.size();i++)
	{
		Snakes[i]->Move(dx, dy);
	}
}

void Controller::AddSnake(Snake& snake)
{
	Snakes.push_back(&snake);
}

void Controller::SetControls(sf::Keyboard::Key left, sf::Keyboard::Key right, sf::Keyboard::Key up, sf::Keyboard::Key down)
{
	Controls[0] = left; Controls[1] = right; Controls[2] = up; Controls[3] = down;
}

void RemoteControllerReceiver::OnReceive(char* buffer, int size)
{
	Keyboard::Key key = *((Keyboard::Key*)buffer);

	if (key == Controls[0]) MoveSnakes(-1, 0);
	else if (key == Controls[1]) MoveSnakes(1, 0);
	else if (key == Controls[2]) MoveSnakes(0, -1);
	else if (key == Controls[3]) MoveSnakes(0, 1);
}


void RemoteControllerSender::OnKeyDown(sf::Keyboard::Key key)
{
	Receiver->Send((char*)&key, sizeof(key));
}


