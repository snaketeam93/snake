#pragma once
#include<string>
#include"Base.h"
#include"Game.h"

namespace APPLICATION
{
	class Application
	{
	private:
		sf::RenderWindow WindowApp;
		GAME::Game SnakeGame;
		float FPS;

		void UpdateEvents();
		void UpdateGame();

		void Update(float dt);
		void Draw();
	public:
		void Init(int w, int h, const std::string& title);
		void Run();
		void Release();
	};
};
