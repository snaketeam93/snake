#include"Application.h"
#include"Timer.h"
#include<sstream>

using namespace APPLICATION;
using namespace sf;
using namespace std;

sf::RenderWindow* DrawableObject::Window;
vector<KeyboardListener*> KeyboardListener::Listeners;

string FloatToString(float f)
{
	stringstream stream;
	stream << f;
	return stream.str();
}

void Application::Init(int w, int h, const string& title)
{
	NETWORK::InitNetwork();

	sf::ContextSettings settings;
	settings.antialiasingLevel = 8;

	WindowApp.create(sf::VideoMode(w, h), title, Style::Default, settings);
	WindowApp.setFramerateLimit(60);
	DrawableObject::Window = &WindowApp;

	SnakeGame.Init(2);
}

void Application::UpdateEvents()
{
	Event event;
	while (WindowApp.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
			WindowApp.close();

		if (event.type == Event::KeyPressed)
		{
			for (int i=0;i<KeyboardListener::Listeners.size();i++)
			{
				if (KeyboardListener::Listeners[i]->Active) KeyboardListener::Listeners[i]->OnKeyDown(event.key.code);
			}
		}

		if (event.type == Event::KeyReleased)
		{
			for (int i=0;i<KeyboardListener::Listeners.size();i++)
			{
				if (KeyboardListener::Listeners[i]->Active) KeyboardListener::Listeners[i]->OnKeyUp(event.key.code);
			}
		}
	}
}

void Application::Update(float dt)
{
	UpdateEvents();
	SnakeGame.Update(dt);
}

void Application::Draw()
{
	WindowApp.clear();
	SnakeGame.Draw();
	WindowApp.display();
}

void Application::Run()
{
	static Timer FrameTimer;
	FrameTimer.Start();

	while (WindowApp.isOpen())
	{
		FrameTimer.Update();
		Update(FrameTimer.GetDt());
		Draw();

		WindowApp.setTitle(FloatToString(FrameTimer.GetFPS()));
	}
	SnakeGame.End();
}

void Application::Release()
{
	SnakeGame.Release();

	NETWORK::ReleaseNetwork();
}