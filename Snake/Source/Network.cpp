﻿#include"Network.h"

#include "Controller.h"
#include<string>

using namespace NETWORK;
using namespace std;
using namespace sf;



void NETWORK::InitNetwork()
{
	WSADATA wsaData;
	WSAStartup( MAKEWORD(2,2), &wsaData);
}

void NETWORK::ReleaseNetwork()
{
	WSACleanup();
}

u_long NETWORK::ResolveHost( const string &host )
{
	LPHOSTENT hostEntry = gethostbyname(host.c_str());

	if ( !hostEntry )
	{
		unsigned int addr = inet_addr( host.c_str() );
		hostEntry = gethostbyaddr((char *)&addr, 4, AF_INET);

		if ( !hostEntry )
		{
			return 0;
		}
	}

	return *((int*)*hostEntry->h_addr_list);
}



Socket::Socket()
{
	socketHandle = NULL;
}

Socket::Socket(SOCKET sock)
{
	socketHandle = sock;
	//getsockname(socketHandle, (sockaddr *)&saddr, &len);
}

bool Socket::Connect(const string& host)
{
	return Connect(ResolveHost(host));
}

bool Socket::Connect(u_long host)
{
	memset( (void*)&saddr, 0, sizeof(saddr) );
	saddr.sin_addr.S_un.S_addr = host;
	saddr.sin_family = AF_INET;
	saddr.sin_port = htons(27015);


	socketHandle = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if ( connect(socketHandle, (sockaddr*)&saddr, sizeof(sockaddr)) == SOCKET_ERROR )
	{
		socketHandle = NULL;
		return false;
	}

	return true;
}

void Socket::Send(char* data, int size)
{
	send(socketHandle, data, size, 0);

	for (SocketListener* sl : Listeners)
	{
		if (sl->IsActive()) sl->OnSend(data, size);
	}
}

void Socket::Receive(char* buffer, int size)
{
		int res = recv(socketHandle, buffer, size, 0);
		if (res > 0)
		{
			for (SocketListener* sl : Listeners)
			{
				if (sl->IsActive()) sl->OnReceive(buffer, size);
			}
		}
		if (res == 0) return;
}

void Socket::AddListener(SocketListener* listener)
{
	Listeners.push_back(listener);
}

void Socket::Close()
{
	if (socketHandle) closesocket(socketHandle);
}

bool ServerSocket::Connect(u_long host)
{
	return false;
}

void ServerSocket::Init()
{
	socketHandle = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	memset( (void*)&saddr, 0, sizeof(saddr) );
	saddr.sin_family = AF_INET;
	saddr.sin_port = htons(27015);
	saddr.sin_addr.s_addr = htonl(INADDR_ANY);

	bind(socketHandle, (sockaddr*)&saddr, sizeof(saddr));
	listen(socketHandle, 10);
}

Socket* ServerSocket::GetClient()
{
	SOCKET s = accept(socketHandle,NULL,NULL);
	return new Socket(s);
}
