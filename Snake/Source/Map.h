#pragma once
#include<vector>
#include"Math.h"
#include"GameObject.h"

namespace GAME
{
	class MapObject
	{
		friend class Map;
	public:
		enum TYPE { FOOD, STAR };

		const MATH::Vec2& GetPos() const { return Pos; }
		const MATH::Vec2 GetV()	const	{ return V; }
		TYPE GetType() const { return Type; }

		void SetPos(const MATH::Vec2& p) { Pos = p; }
		void SetV(const MATH::Vec2& v) 
		{ 
			V = v; 
			OnChangeV();
		}

		virtual void OnChangeV() {}
	protected:
		static class Map* MapObj;

		MapObject(TYPE t) : Type(t) {}

		TYPE Type;
		MATH::Vec2 Pos;
		MATH::Vec2 V;
	};

	class FoodObject : public MapObject
	{
	public:
		static const int Width = 10;
		static const int MaxSpeed = 100;
		static const int MinSpeed = 50;

		int Speed;
		int Index;

		FoodObject() : MapObject(MapObject::FOOD) {}

		void OnChangeV();
	};

	class Map : public GameObject
	{
		friend class Game;
	private:
		MATH::Vec2 Size;

		std::vector<FoodObject> Food;

	public:
		void Create(int w, int h, int maxFood=10);
		void GenerateFood();
		void NewFood(int i, int generator, int generator2);
		void Update(float dt);

		MATH::Vec2			GetSize()			const { return Size;		}
		int					GetFoodCount()		const { return Food.size(); }
		const FoodObject&	GetFood(int i)		const { return Food[i];		}
	};
};


