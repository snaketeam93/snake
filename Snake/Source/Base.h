#pragma once
#include<vector>
#include<SFML\Graphics.hpp>

namespace APPLICATION
{
	class DrawableObject 
	{
		friend class Application;
	protected:
		static sf::RenderWindow* Window;
	};

	class KeyboardListener
	{
		friend class Application;
	protected:
		static std::vector<KeyboardListener*> Listeners;
		bool Active;
	public:
		KeyboardListener() : Active(false) { Listeners.push_back(this); }

		void StartListenKeyboard() { Active = true; }
		void StopListenKeyboard() { Active = false; }

		virtual void OnKeyDown(sf::Keyboard::Key key) {};
		virtual void OnKeyUp(sf::Keyboard::Key key) {};
	};
};