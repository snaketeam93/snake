#pragma once
#include<vector>

namespace GAME
{
	class GameEvent
	{
		friend class Game;
	public:
		enum TYPE { STARTGAME, NEWFOOD, CHANGEDIR, SYNCHHEAD, EATFOOD, CONTROLPOINT, FOODDIR, SYNCHSNAKE, SYNCHFOOD };

		GameEvent() {}
		GameEvent(TYPE t, int numPl, int maxF) : Type(t), numPlayer(numPl), maxFood(maxF) {}
		GameEvent(TYPE t, int index, float x, float y) :Type(t)
		{
				 if (t == NEWFOOD) { foodIndex = index; generator = x; generator2 = y; }
			else if (t == CHANGEDIR) { snakeIndex = index; dx = x; dy = y; }
			else if (t == SYNCHHEAD) { snakeHeadIndex = index; hx = x; hy = y; }
			else if (t == EATFOOD) { eaterIndex = index; efx = x; efy = y; }
			else if (t == CONTROLPOINT) { snakeIndex2 = index; px = x; py = y; }
			else if (t == FOODDIR) { foodDirIndex = index; fdx = x; fdy = y; }
		}

		GameEvent(TYPE t, int index, int param, float* data) : Type(t)
		{
			if (t == SYNCHSNAKE) { snake = index; snakeElements = param; snakeData = data;  }
			else if (t == SYNCHFOOD) { foodCount = index; foodData = data;  }
		}

		TYPE Type;
		union
		{
			//STARTGAME params
			struct { int numPlayer; int maxFood; };
			//NEWFOOD params
			struct { int foodIndex; int generator; int generator2; };
			//FOODDIR params
			struct { int foodDirIndex; float fdx; float fdy; };
			//CHANGEDIR
			struct { int snakeIndex; int dx; int dy; };
			//SYNHHEAD
			struct { int snakeHeadIndex; float hx; float hy; };
			//EATFOOD
			struct { int eaterIndex; int efx; int efy; };
			//CONTROLPOINT
			struct { int snakeIndex2; int px; int py; };
			//SYNCHSNAKES
			struct { int snake; int snakeElements; float* snakeData; };
			//SYNCHFOOD
			struct { int foodCount; float* foodData; };
		};
	};

	class GameEventListener
	{
		friend class Game;
	protected:
		static std::vector<GameEventListener*> Listeners;
	public:
		GameEventListener() { Listeners.push_back(this); }

		virtual void OnReceive(GameEvent gevent) = 0;
		virtual ~GameEventListener() {}
	};

	
	class GameObject
	{
		friend class Game;
	protected:
		static Game* GameObj;
	public:
		Game* GetGame() { return GameObj; }
	};
};
