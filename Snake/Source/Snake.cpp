#include "Snake.h"
#include"Game.h"

using namespace std;
using namespace sf;
using namespace GAME;
using namespace MATH;

ControlPoint& Snake::GetElement(int i)
{
	//if (i >= Points.size()) return ;
	return Points[i];
	/*
	list<ControlPoint>::iterator it;
	if (i <= Points.size() / 2){
	it = Points.begin();
	for (int j=0;j<i;j++)
	it++;
	}
	else{
	it = Points.end();
	for (int j=Points.size()-1;j>=i;j--)
	it--;
	}

	return *it;*/
}
/*
Segment& Snake::GetSegment(int i)
{
list<Segment>::iterator it;
if (i <= Segments.size() / 2){
it = Segments.begin();
for (int j=0;j<i;j++)
it++;
}
else{
it = Segments.end();
for (int j=Segments.size()-1;j>=i;j--)
it--;
}

return *it;
}*/

void Snake::Move(int dx, int dy)
{
	if (Points[0].Dir != Vec2(dx, dy))
	{
		Moved = true;

		//Points[0].Dir = Vec2(dx, dy);

		//ControlPoint sec = GetElement(1);
		//sec.Length = (sec.Pos - Points.front().Pos).Length();

		ControlPoint cp = Points[0];
		cp.Dir = Vec2(dx, dy);
		cp.Length = 0.0;
		AddElementFront(cp);

		Points[1].Dir = Vec2(dx, dy);
	}

	int key = 0;
	if (dx == -1 && dy == 0) key = 0;
	else if (dx == 1 && dy == 0) key = 1;
	else if (dx == 0 && dy == -1) key = 2;
	else if (dx == 0 && dy == 1) key = 3;

	int pos = (int)Points[0].Pos.y*1000+(int)Points[0].Pos.x;

	//GameObj->SendEvent(GameEvent(GameEvent::CONTROLPOINT, Index, Points[1].Pos.x, Points[1].Pos.y));
	GameObj->SendEvent(GameEvent(GameEvent::CHANGEDIR, Index, key, pos));

	//GameObj->SendEvent(GameEvent(GameEvent::SYNCHHEAD, Index, Points[0].Pos.x, Points[0].Pos.y));
}
/*
MATH::Rect Snake::CreateSegment(const ControlPoint& cp1, const ControlPoint& cp2)
{
float x = min(cp1.Pos.x, cp2.Pos.x) - Width*0.5;
float y = min(cp1.Pos.y, cp2.Pos.y) - Width*0.5;
float w = abs(cp1.Pos.x - cp2.Pos.x) + Width;
float h = abs(cp1.Pos.y - cp2.Pos.y) + Width;

return MATH::Rect(x, y, w, h);
}
*//*
void Snake::UpdateSegment(int i)
{
ControlPoint cp1 = GetElement(i);
ControlPoint cp2 = GetElement(i+1);

float x = min(cp1.Pos.x, cp2.Pos.x) - Width*0.5;
float y = min(cp1.Pos.y, cp2.Pos.y) - Width*0.5;
float w = abs(cp1.Pos.x - cp2.Pos.x) + Width;
float h = abs(cp1.Pos.y - cp2.Pos.y) + Width;


}*/

void Snake::Sync(const Vec2& head)
{
	Vec2 h = GetHead().Pos;
	Vec2 d = head - h;


	//Points[0].Pos = head;// d;
	for (ControlPoint& p : Points)
		p.Pos += d;
}

void Snake::AddElementBack(ControlPoint v)
{
	Points.push_back(v);

	if (Points.size() > 1)
	{
		//Segments.push_back(CreateSegment(GetElement(Points.size()-2), GetElement(Points.size()-1)));
	}
}
void Snake::AddElementFront(ControlPoint v)
{
	Points.insert(Points.begin(), v);

	if (Points.size() > 1)
	{
		//Segments.push_front(CreateSegment(0), GetElement(1)));
	}
}

int Snake::GetLength(int el)
{
	if (el == -1) el = Points.size()-1;

	int l = 0;
	for (ControlPoint& p : Points)
		l += p.Length;

	return l;
}

void Snake::UpdateLength()
{
	float prevDistance = 0.0;
	float distance = 0.0;
	int index = 0;
	for (ControlPoint& cp : Points)
	{
		prevDistance = distance;
		distance += cp.Length;

		if (distance > Length)
		{
			float d = distance - Length;

			tailMove = true;
			cp.Pos += cp.Dir*d;
			cp.Length -= d;
			return;
		}

		if (index == Points.size()-1)
		{
			if (distance < Length)
			{
				float d = distance - Length;

				//p2->Pos = p2->Pos - p2->Dir*d;
				tailMove = false;
			}
		}
		index++;
	}
}

bool Snake::Collide(const Vec2& pos, int w, int h, Vec2& pushVec)
{
	MATH::Rect r, r2;

	r2.x = pos.x;
	r2.y = pos.y;
	r2.w = w;
	r2.h = h;
	bool col = false;
	for (int i=0;i<Points.size()-1;i++)
	{
		ControlPoint& p1 = Points[i];
		ControlPoint& p2 = Points[i+1];

		if (p1.Pos.y == p2.Pos.y)
		{
			r.x = min(p1.Pos.x, p2.Pos.x);
			r.y = p1.Pos.y;
			r.h = Width;
			r.w = p2.Length;

			col = Test::RectRect(r, r2, pushVec);
			//pushDir = Vec2(0.0, r2.GetCenter().y > r.GetCenter().y ? 1.0 : -1.0);
		}
		else if (p1.Pos.x == p2.Pos.x)
		{
			r.x = p1.Pos.x;
			r.y = min(p1.Pos.y, p2.Pos.y);
			r.w = Width;
			r.h = p2.Length;

			col = Test::RectRect(r, r2, pushVec);

			//pushDir = Vec2(r2.GetCenter().x > r.GetCenter().x ? 1.0 : -1.0, 1.0);
		}

		if (col) return true;
	}

	return false;
}

void Snake::Update(double dt)
{
	TimeStep += dt;

	Vec2 dS = Points[0].Dir*dt*Speed;
	Points[0].Pos += dS;
	Points[1].Length += dS.Length();

	if (tailMove)
	{
		Points[Points.size()-1].Pos += Points[Points.size()-1].Dir*dt*Speed;
		Points[Points.size()-1].Length -= dS.Length();
	}

	TimeStep += dt;
	if (TimeStep >= 0.3)
	{
		UpdateLength();
		TimeStep = 0;
	}

	/*

	float length0 = GetLength(Points.size()-1);
	float length1 = GetLength(Points.size()-2);

	float length = Points.size() > 2 ? (length0 - length1) : last.Length;

	last.Pos = prevLast.Pos - last.Dir*length;
	last.Length = length;*/


	if (Points.size() > 2)
	{
		ControlPoint p0 = Points[Points.size()-1];	//ostatni
		ControlPoint p1 = Points[Points.size()-2]; // przedostatni

		if (p0.Dir.x > 0 && p0.Pos.x >= p1.Pos.x ||
			p0.Dir.x < 0 && p0.Pos.x <= p1.Pos.x ||
			p0.Dir.y > 0 && p0.Pos.y > p1.Pos.y ||
			p0.Dir.y < 0 && p0.Pos.y < p1.Pos.y)
		{
			Points.pop_back();
		}
	}

	/*
	if (TimeStep >= 1.0/Speed || Moved)
	{
	Vector2i pos = Elements.front()+Dir;
	Elements.push_front(pos);
	Elements.pop_back();

	if (Moved) TimeStep = 0;
	else TimeStep -= 1.0/Speed;

	Moved = false;
	GameObj->SendEvent(GameEvent(GameEvent::SNAKEMOVE, Index, pos.x, pos.y));
	}*/

	//_sleep(10);

}





