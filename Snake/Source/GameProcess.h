#pragma once
#include<thread>
#include<vector>
#include"GameObject.h"
#include"Network.h"

namespace GAME
{
	namespace SynchData
	{

	};

	class GameProcess
	{
	protected:
		class Game* GameObj;
	public:
		GameProcess(Game* game) : GameObj(game) {}

		virtual void Release() {}
		virtual ~GameProcess() {}
	};

	class ClientGameProcess : public GameProcess, public NETWORK::SocketListener
	{
	private:
		NETWORK::Socket Server;
		std::thread* ServerGameEventListener;
		GameEvent LastEvent;
	public:
		ClientGameProcess(Game* game, const std::string& host);
		NETWORK::Socket* GetServerSocket() { return &Server; }

		void OnReceive(char* buffer, int size);
		void OnSend(char* data, int size) {}

		void operator()();

		void Release();
	};

	class ServerGameProcess : public GameProcess, public GameEventListener
	{
	private:
		class ClientListenerThread
		{
		private:
			std::thread* ListenerThread;
			NETWORK::Socket* Client;
		public:
			ClientListenerThread(NETWORK::Socket* c) : Client(c) {}

			void Start()
			{
				ListenerThread = new std::thread(*this);
			}

			void operator()()
			{
				while(1)
				{
					char key[4];
					Client->Receive(key, sizeof(key));
				}
			}

			void Release()
			{
				ListenerThread->detach();
				delete ListenerThread;
			}
		};

		std::vector<NETWORK::Socket*> Clients;
		std::vector<ClientListenerThread> Threads;
	public:
		ServerGameProcess(Game* game);

		NETWORK::Socket* GetClientSocket(int i) { return Clients[i]; }

		void OnReceive(GameEvent ev);

		void Release();
	};

};