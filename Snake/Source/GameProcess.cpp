#include"Game.h"
#include"GameProcess.h"

using namespace GAME;
using namespace NETWORK;
using namespace std;

ClientGameProcess::ClientGameProcess(Game* game, const string& host) : GameProcess(game) 
{
	Server.AddListener(this);
	StartListenSocket();	

	if (!Server.Connect(host))
	{
		MessageBox(0, ("Nie po��czono z "+host).c_str(), "Info", MB_ICONINFORMATION);
	}

	ServerGameEventListener = new thread(*this);
}

void ClientGameProcess::operator()()
{
	while(1)
	{
		GameEvent gameEvent;
		Server.Receive((char*)&gameEvent, sizeof(GameEvent));
		LastEvent = gameEvent;

		if (gameEvent.Type == GameEvent::SYNCHSNAKE)
		{
			float* pos = new float[gameEvent.snakeElements*2];
			Server.Receive((char*)pos, sizeof(float)*gameEvent.snakeElements*2);
			gameEvent.snakeData = pos;
		}
		else if (gameEvent.Type == GameEvent::SYNCHFOOD)
		{
			float* pos = new float[gameEvent.foodCount*2];
			Server.Receive((char*)pos, sizeof(float)*gameEvent.foodCount*2);
			gameEvent.foodData = pos;
		}

		GameObj->Update(gameEvent);
	}
}

void ClientGameProcess::OnReceive(char* buffer, int size)
{
	//if (LastEvent.Type == GameEvent::SYNCHSNAKE) return;

	//GameEvent gameEvent = *((GameEvent*)buffer);
	//GameObj->Update(gameEvent);
}

void ClientGameProcess::Release()
{
	ServerGameEventListener->detach();
	delete ServerGameEventListener;
	Server.Close();
}

ServerGameProcess::ServerGameProcess(Game* game) : GameProcess(game)
{
	ServerSocket serverSocket;
	serverSocket.Init();

	for (int i=0;i<GameObj->GetPlayersCount()-1;i++)
	{
		Clients.push_back(serverSocket.GetClient());
		Threads.push_back(ClientListenerThread(Clients[i]));
		Threads[i].Start();
	}

	serverSocket.Close();
}


void ServerGameProcess::Release()
{
	for (ClientListenerThread& th : Threads)
	{
		th.Release();
	}
	for (Socket* client : Clients)
	{
		client->Close();
		delete client;
	}
}

void ServerGameProcess::OnReceive(GameEvent ev)
{
	for (Socket* client : Clients)
	{
		client->Send((char*)&ev, sizeof(GameEvent));

		if (ev.Type == GameEvent::SYNCHSNAKE)
		{
			client->Send((char*)ev.snakeData, sizeof(float)*2*ev.snakeElements);
			delete[] ev.snakeData;
		}

		else if (ev.Type == GameEvent::SYNCHFOOD)
		{
			client->Send((char*)ev.foodData, sizeof(float)*2*ev.foodCount);
			delete[] ev.foodData;
		}
	}
}