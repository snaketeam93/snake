#include "Map.h"
#include"Game.h"

using namespace GAME;
using namespace MATH;

Map* MapObject::MapObj = NULL;

void Map::Create(int w, int h, int maxFood)
{
	MapObject::MapObj = this;
	Size = Vec2(w, h);
	Food.resize(maxFood);
	for (int i=0;i<maxFood;i++) Food[i].Index = i;
}

void Map::GenerateFood()
{
	srand(time(NULL));
	for (int i=0;i<Food.size();i++)
	{
		NewFood(i, rand(), rand());
	}
}

void Map::NewFood(int i, int generator, int generator2)
{
	Food[i].Pos = Vec2(generator%(int)Size.x, generator2%(int)Size.y);
	Food[i].Speed = (generator*generator2)%(FoodObject::MaxSpeed-FoodObject::MinSpeed)+FoodObject::MinSpeed;
	GameObj->SendEvent(GameEvent(GameEvent::NEWFOOD, i, generator, generator2));
	Food[i].SetV((Vec2(generator2%100, generator%100))/100.0);
}

void Map::Update(float dt)
{
	
	for (FoodObject& fo : Food)
	{
		fo.Pos += fo.V * dt * fo.Speed;

		//if (fo.Pos.x < 0 || fo.Pos.x+FoodObject::Width > Size.x) fo.V.x = -fo.V.x;
		//if (fo.Pos.y < 0 || fo.Pos.y+FoodObject::Width > Size.y) fo.V.y = -fo.V.y;
	}
}

void FoodObject::OnChangeV()
{
	MapObj->GetGame()->SendEvent(GameEvent(GameEvent::FOODDIR, Index, V.x, V.y));
}



