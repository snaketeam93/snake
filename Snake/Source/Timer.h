#pragma once

namespace APPLICATION
{
	class Timer
	{
	private:
		enum STATE { ST_RUN, ST_PAUSE };

		float Time;
		float LastTime;
		float StartTime;
		float FPS;
		int Frames;
		float Dt;
		STATE State;
	public:
		Timer() : State(ST_PAUSE), Time(0.0f), LastTime(0.0f), FPS(0.0f), Dt(0.0f), Frames(0) {}
		~Timer() {}

		void Start();
		void Stop();
		void Update();

		float GetSecs();
		float GetFPS() { return FPS; }
		float GetDt()  { return Dt;  }
	};
}