#include"Game.h"
#include<list>
#include<thread>
#include<math.h>

using namespace NETWORK;
using namespace MATH;
using namespace GAME;
using namespace std;
using namespace sf;

StartState Game::StartPoints[4];
Color Game::SnakesColors[4];
Game* GameObject::GameObj = NULL;
vector<GameEventListener*> GameEventListener::Listeners;

void Game::OnKeyDown(sf::Keyboard::Key key)
{
	if (key == sf::Keyboard::V)
	{
		SendEvent(GameEvent(GameEvent::SYNCHHEAD, 1, Snakes[1].Points[0].Pos.x, Snakes[1].Points[0].Pos.y));
	}
}

void Game::Update(GameEvent ev)
{
	if (bEnd) return;
	if (ev.Type == GameEvent::CONTROLPOINT)
	{
		//Snakes[ev.snakeIndex2].Points[0].Pos.x = ev.px;
		//Snakes[ev.snakeIndex2].Points[0].Pos.y = ev.py;
	}
	else if (ev.Type == GameEvent::SYNCHHEAD)
	{
		Snakes[ev.snakeHeadIndex].Sync(Vec2(ev.hx, ev.hy));
		//Snakes[ev.snakeHeadIndex].Update(0.1);
	}
	else if (ev.Type == GameEvent::SYNCHSNAKE)
	{
		int j=0;
		for (int i=0;i<ev.snakeElements;i++)
		{
			if (i >= Snakes[ev.snake].Points.size()) continue;
			ControlPoint& cp = Snakes[ev.snake].GetElement(i);
			cp.Pos.x = ev.snakeData[j];
			cp.Pos.y = ev.snakeData[j+1];
			j+=2;
		}

		delete[] ev.snakeData;
	}
	else if (ev.Type == GameEvent::SYNCHFOOD)
	{
		int j=0;
		for (int i=0;i<ev.foodCount;i++)
		{
			if (i >= MapObj.Food.size()) continue;

			FoodObject& f = MapObj.Food[i];
			f.SetPos(Vec2(ev.foodData[j], ev.foodData[j+1]));
			j+=2;
		}

		delete[] ev.foodData;
	}
	else if (ev.Type == GameEvent::NEWFOOD)
	{
		if (ev.foodIndex < MapObj.Food.size())
		{
			MapObj.NewFood(ev.foodIndex, ev.generator, ev.generator2);
			//MapObj.Food[ev.foodIndex].SetPos(Vec2(ev.fx, ev.fy));
		}
	}
	else if (ev.Type == GameEvent::FOODDIR)
	{
		if (ev.foodDirIndex < MapObj.Food.size())
		{
			MapObj.Food[ev.foodDirIndex].SetV(Vec2(ev.fdx, ev.fdy));
		}
	}
	else if (ev.Type == GameEvent::CHANGEDIR)
	{
		if (ev.snakeIndex < Snakes.size())
		{
			int dx, dy;
			if (ev.dx == 0) { dx = -1; dy = 0; }
			else if (ev.dx == 1) { dx = 1; dy = 0; }
			else if (ev.dx == 2) { dx = 0; dy = -1; }
			else if (ev.dx == 3) { dx = 0; dy = 1; }

			int x, y;
			y = ev.dy / 1000.0;
			x = ev.dy - y*1000.0;

			Snakes[ev.snakeIndex].Points[0].Pos.x = x;
			Snakes[ev.snakeIndex].Points[0].Pos.y = y;
			Snakes[ev.snakeIndex].Move(dx, dy);

			//Snakes[ev.snakeIndex].Move(ev.dx, ev.dy);
		}
	}
	else if (ev.Type == GameEvent::STARTGAME)
	{
		Create(ev.numPlayer, ev.maxFood);
	}
	else if (ev.Type == GameEvent::EATFOOD)
	{
		if (ev.eaterIndex < Snakes.size())
			Snakes[ev.eaterIndex].Length += 10;
	}
}


void Game::SendEvent(GameEvent gameEvent)
{
	for (GameEventListener* listener : GameEventListener::Listeners)
	{
		listener->OnReceive(gameEvent);
	}
}

void Game::Create(int numPlayers, int maxFood)
{
	PlayersCount = numPlayers;
	StartListenKeyboard();

	Vec2 mapSize = Vec2(580, 580);
	Vec2 startPadding = Vec2(50, 20);

	StartPoints[0].Pos = startPadding;
	StartPoints[0].Dir = Vec2(1, 0);
	StartPoints[1].Pos = Vec2(mapSize.x-startPadding.x, 1);
	StartPoints[1].Dir = Vec2(0, 1);
	StartPoints[2].Pos = mapSize - startPadding;
	StartPoints[2].Dir = Vec2(-1, 0);
	StartPoints[3].Pos = Vec2(1, mapSize.y-startPadding.y);
	StartPoints[3].Dir = Vec2(0, -1);

	SnakesColors[0] = Color(255, 0, 0, 255);
	SnakesColors[1] = Color(0, 255, 0, 255);
	SnakesColors[2] = Color(0, 0, 255, 255);
	SnakesColors[3] = Color(255, 255, 0, 255);

	MapObj.Create(mapSize.x, mapSize.y, maxFood);

	Snakes.resize(numPlayers);
	for (int i=0;i<numPlayers;i++)
	{
		Snakes[i].Index = i;
		int startLength = 30;
		int startWidth = 10;

		Snakes[i].SetWidth(startWidth);
		Snakes[i].SetLength(startLength);
		Snakes[i].AddElementBack(ControlPoint(StartPoints[i].Pos, StartPoints[i].Dir, 0));
		Snakes[i].AddElementBack(ControlPoint(StartPoints[i].Pos-StartPoints[i].Dir*startLength, StartPoints[i].Dir, startLength));

		//for (int j=startLength-1;j>=0;j--)
		{
			//	Snakes[i].Elements.push_back(Vector2i(StartPoints[i].Pos+StartPoints[i].Dir*j));
		}

		Snakes[i].Col = SnakesColors[i];
	}

	SendEvent(GameEvent(GameEvent::STARTGAME, numPlayers, maxFood));

	MapObj.GenerateFood();
}

void Game::Init(int numPlayers)
{
	GameObject::GameObj = this;
	bEnd = false;
	Type = CLIENT;
	if (Type == SERVER)
	{
		PlayersCount = numPlayers;
		GameProc = new ServerGameProcess(this);
		Create(numPlayers, 10);

		LocalController* LocalCont = new LocalController;
		LocalCont->StartListenKeyboard();
		LocalCont->SetControls(Keyboard::Left, Keyboard::Right, Keyboard::Up, Keyboard::Down);

		LocalController* LocalContWSAD = new LocalController;
		LocalContWSAD->StartListenKeyboard();
		LocalContWSAD->SetControls(Keyboard::A, Keyboard::D, Keyboard::W, Keyboard::S);

		LocalCont->AddSnake(Snakes[0]);
		LocalContWSAD->AddSnake(Snakes[0]);

		Controllers.push_back(LocalCont);
		Controllers.push_back(LocalContWSAD);

		for (int i=0;i<PlayersCount-1;i++)
		{
			RemoteControllerReceiver* RemoteCont = new RemoteControllerReceiver;
			RemoteCont->SetControls(Keyboard::Left, Keyboard::Right, Keyboard::Up, Keyboard::Down);
			((ServerGameProcess*) GameProc)->GetClientSocket(i)->AddListener(RemoteCont);
			RemoteCont->StartListenSocket();

			RemoteControllerReceiver* RemoteContWSAD = new RemoteControllerReceiver;
			RemoteContWSAD->SetControls(Keyboard::A, Keyboard::D, Keyboard::W, Keyboard::S);
			((ServerGameProcess*) GameProc)->GetClientSocket(i)->AddListener(RemoteContWSAD);
			RemoteContWSAD->StartListenSocket();

			RemoteCont->AddSnake(Snakes[i+1]);
			RemoteContWSAD->AddSnake(Snakes[i+1]);

			Controllers.push_back(RemoteCont);
			Controllers.push_back(RemoteContWSAD);
		}
	}
	if (Type == CLIENT)
	{
		//GameProc = new ClientGameProcess(this, "192.168.32.1");
		GameProc = new ClientGameProcess(this, "169.254.149.133");
		//GameProc = new ClientGameProcess(this, "localhost");

		RemoteControllerSender* RemoteCont = new RemoteControllerSender;
		RemoteCont->StartListenKeyboard();
		RemoteCont->SetReceiver(((ClientGameProcess*)GameProc)->GetServerSocket());
		Controllers.push_back(RemoteCont);
	}
}

void Game::CheckCollisions()
{
	//jedzenie
	Vec2 pushVec;
	for (int k=0;k<Snakes.size();k++)
	{
		for (int i=0;i<MapObj.GetFoodCount();i++)
		{
			Vec2 p = Snakes[k].Points.front().Pos;
			MATH::Rect r = MATH::Rect(p.x - Snakes[k].Width*0.5, p.y - Snakes[k].Width*0.5, Snakes[k].Width, Snakes[k].Width);
			Vec2 pos = MapObj.GetFood(i).GetPos() + Vec2(FoodObject::Width/2, FoodObject::Width/2);
            MATH::Rect r2 = MATH::Rect(pos.x - Snakes[k].Width*0.5, pos.y - Snakes[k].Width*0.5, Snakes[k].Width, Snakes[k].Width);

			if (MATH::Test::RectRect(r, r2, pos))
			{
				Snakes[k].Length += 10;
				SendEvent(GameEvent(GameEvent::EATFOOD, k, pos.x, pos.y));
				if (Type == SERVER) MapObj.NewFood(i, rand(), rand());
			}

			if (Snakes[k].Collide(MapObj.Food[i].GetPos(), 10, 10, pushVec))
			{
			    Vec2 dirVec(1.0, 1.0);
			    if (pushVec.y != 0.0) dirVec.y = -1;
			    if (pushVec.x != 0.0) dirVec.x = -1;

			    MapObj.Food[i].SetPos(MapObj.Food[i].GetPos()+pushVec);
				MapObj.Food[i].SetV(MapObj.Food[i].GetV()*dirVec);
			}
		}
	}
}

void Game::DrawMap(int x, int y)
{
	RectangleShape mapRect(Vector2f(MapObj.Size.x, MapObj.Size.y));
	mapRect.setPosition(x, y);
	mapRect.setFillColor(Color(0, 100, 0, 255));
	Window->draw(mapRect);

	CircleShape foodCircle(FoodObject::Width/2);
	foodCircle.setFillColor(Color(255, 255, 0, 255));
	foodCircle.setOutlineColor(Color(0, 0, 0, 255));
	foodCircle.setOutlineThickness(2);

	for (int i=0;i<MapObj.Food.size();i++)
	{
		Vec2 pos = MapObj.Food[i].GetPos();
		foodCircle.setPosition(pos.x+x, pos.y+y);
		Window->draw(foodCircle);
	}
}

void DrawLine(Vec2& p1, Vec2& p2, int width, RenderWindow* wnd)
{

}

void Game::DrawSnakes(int x, int y)
{
	//---------------------------
	//RYSOWANIE PUNKTOW ZGIECIA
	//---------------------------
	/*
	float r = 6.0;
	CircleShape circleShape(r);
	circleShape.setFillColor(sf::Color(0, 0, 255, 255));
	for (Snake& s : Snakes)
	{
		for (ControlPoint& cp : s.Points)
		{
			circleShape.setPosition(cp.Pos.x-r*0.5, cp.Pos.y-r*0.5);
			Window->draw(circleShape);
		}
	}*/

	int border = 2;
	RectangleShape rectShape;
	RectangleShape innerRectShape;
	rectShape.setFillColor(Color(0,0,0,255));
	for (Snake& s : Snakes)
	{
		for (int i=0;i<s.Points.size()-1;i++)
		{
			Vec2 p1 = s.Points[i].Pos;
			Vec2 p2 = s.Points[i+1].Pos;

			int l = (p1 - p2).Length()+s.Width;

			if (p1.x == p2.x) rectShape.setSize(Vector2f(s.Width, l));
			else rectShape.setSize(Vector2f(l, s.Width));

			rectShape.setPosition(min(p1.x, p2.x)-s.Width/2+x, min(p1.y, p2.y)-s.Width/2+y);
			Window->draw(rectShape);
		}

		for (int i=0;i<s.Points.size()-1;i++)
		{
			Vec2 p1 = s.Points[i].Pos;
			Vec2 p2 = s.Points[i+1].Pos;

			innerRectShape.setFillColor(s.Col);
			int l = (p1 - p2).Length()+s.Width;
			int border = 2;

			if (p1.x == p2.x) innerRectShape.setSize(Vector2f(s.Width-border*2, l-border*2));
			else innerRectShape.setSize(Vector2f(l-border*2, s.Width-border*2));

			innerRectShape.setPosition(min(p1.x, p2.x)-s.Width/2+x+border, min(p1.y, p2.y)-s.Width/2+y+border);
			Window->draw(innerRectShape);
		}
	}

	/*
	for (int i=0;i<Snakes.size();i++)
	{
		Snake snake = Snakes[i];
		vector<ControlPoint>::iterator it = snake.Points.begin();
		Vec2 p1, p2;

		//int border = 2;
		//
		RectangleShape rectShape;
		RectangleShape innerRectShape;;
		float r = snake.Width*0.6;
		CircleShape circleShape(r);
		circleShape.setFillColor(sf::Color(0, 0, 255, 255));
		p1 = it->Pos;
		++it;
		for (;it!=snake.Points.end();it++)
		{
			p2 = it->Pos;
			rectShape.setFillColor(Color(0,0,0,255));

			int l = (p1 - p2).Length()+snake.Width;

			if (p1.x == p2.x)
			{
				rectShape.setSize(Vector2f(snake.Width, l));
			}
			else
			{
				rectShape.setSize(Vector2f(l, snake.Width));
			}

			rectShape.setPosition(min(p1.x, p2.x)-snake.Width/2+x, min(p1.y, p2.y)-snake.Width/2+y);

			Window->draw(rectShape);
			p1 = p2;
		}
		it = snake.Points.begin();
		p1 = it->Pos;
		++it;
		for (;it!=snake.Points.end();it++)
		{
			p2 = it->Pos;

			innerRectShape.setFillColor(snake.Col);
			circleShape.setPosition(p1.x, p1.y);

			int l = (p1 - p2).Length()+snake.Width;
			int border = 2;

			if (p1.x == p2.x)
			{
				innerRectShape.setSize(Vector2f(snake.Width-border*2, l-border*2));
			}
			else
			{
				innerRectShape.setSize(Vector2f(l-border*2, snake.Width-border*2));
			}
			innerRectShape.setPosition(min(p1.x, p2.x)-snake.Width/2+x+border, min(p1.y, p2.y)-snake.Width/2+y+border);

			Window->draw(innerRectShape);
			Window->draw(circleShape);
			p1 = p2;
		}
	}*/
}

void Game::Draw()
{
	DrawMap(10, 10);
	DrawSnakes(10, 10);
}

void Game::Update(float dt)
{
	for (Snake& s : Snakes)
	{
		s.Update(dt);
	}

	MapObj.Update(dt);


	if (Type == SERVER)
	{
		static double SyncTime = 0.0;

		if (SyncTime >= 1.0f)
		{
			//for (int i=0;i<Snakes.size();i++)
				//SynchronizeSnake(i);

			SynchronizeFood();

			SyncTime = 0.0;
		}

		CheckCollisions();

		SyncTime += dt;
	}
}

void Game::Release()
{
	GameProc->Release();
	delete GameProc;

	for (int i=0;i<Controllers.size();i++)
	{
		delete Controllers[i];
	}


}

void Game::SynchronizeSnake(int index)
{
	Snake& snake = Snakes[index];
	float* pos = new float[snake.Points.size()*2];

	int j=0;
	for (int i=0;i<snake.Points.size();i++)
	{
		pos[j] = snake.GetElement(i).Pos.x;
		pos[j+1] = snake.GetElement(i).Pos.y;
		j+=2;
	}

	SendEvent(GameEvent(GameEvent::SYNCHSNAKE, index, snake.Points.size(), pos));
}

void Game::SynchronizeFood()
{
    int j=0;

	float* pos = new float[MapObj.Food.size()*2];

	for (int i=0;i<MapObj.Food.size();i++)
	{
		pos[j] = MapObj.Food[i].GetPos().x;
		pos[j+1] = MapObj.Food[i].GetPos().y;
		j+=2;
	}

	SendEvent(GameEvent(GameEvent::SYNCHFOOD, MapObj.Food.size(), 0, pos));
}
