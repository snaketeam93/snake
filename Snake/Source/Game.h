#pragma once
#include<list>
#include"Base.h"
#include"Map.h"
#include"Snake.h"
#include"Controller.h"
#include"GameProcess.h"

namespace GAME
{
	struct StartState
	{
		MATH::Vec2 Pos;
		MATH::Vec2 Dir;
	};


	class Game : public APPLICATION::DrawableObject, public APPLICATION::KeyboardListener
	{
	private:
		enum TYPE { SERVER, CLIENT };

		TYPE Type;
		bool bEnd;

		static StartState StartPoints[4];
		static sf::Color SnakesColors[4];
		GameProcess* GameProc;

		Map MapObj;
		std::vector<Snake> Snakes;
		std::vector<Controller*> Controllers;
		int PlayersCount;


		void CheckCollisions();

		void DrawMap(int x, int y);
		void DrawSnakes(int x, int y);
	public:
		void OnKeyDown(sf::Keyboard::Key key);

		void End() { bEnd = true; }
		void Init(int numPlayers);
		void Update(float dt);
		void Draw();
		void Release();

		void SynchronizeSnake(int snake);
		void SynchronizeFood();

		void SynchronizeSnake(int snake, float* data, int elements);

		void Create(int numPlayers, int maxFood);

		int GetPlayersCount() const { return PlayersCount; }

		void SendEvent(GameEvent gameEvent);
		void Update(GameEvent gevent);
	};
};
