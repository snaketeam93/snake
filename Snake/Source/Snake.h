#pragma once
#include<list>
#include"Math.h"
#include<SFML\Graphics.hpp>
#include"GameObject.h"


namespace GAME
{
	struct ControlPoint
	{
		MATH::Vec2 Pos;
		MATH::Vec2 Dir;
		float Length;

		ControlPoint() {}
		ControlPoint(MATH::Vec2 p, MATH::Vec2 d, float l) : Pos(p), Dir(d), Length(l) {}
	};

	class Segment
	{
	private:
		ControlPoint* Front;
		ControlPoint* Tail;
		float Length;
		MATH::Rect SegmRect;
	public:
		const ControlPoint& GetFront() const { return *Front; }
		const ControlPoint& GetTail() const { return *Tail; }
		float GetLength()	const { return Length; }
		const MATH::Rect& GetRect() { return SegmRect; }
	};

	class Snake : public GameObject
	{
		friend class Game;
	private:
		//std::list<ControlPoint> Points;
		std::vector<ControlPoint> Points;

		//std::list<sf::Vector2i> Elements;
		int Width;
		float Speed;
		sf::Color Col;
		float TimeStep;
		float Length;
		bool Moved;
		bool tailMove;
		int Index;

		//MATH::Rect CreateSegment(const ControlPoint& cp1, const ControlPoint& cp2);
		void UpdateSegment(int i);

	public:
		void Move(int dx, int dy);
		void Update(double dt);
		void Draw(int x, int y);

		const ControlPoint& GetHead() const { return Points[0]; }
		const ControlPoint& GetTail() const { return Points[Points.size()-1]; }

		ControlPoint& GetElement(int i);

		bool Collide(const MATH::Vec2& pos, int w, int h, MATH::Vec2& pushVec);

		void Sync(const MATH::Vec2& head);
		void AddElementBack(ControlPoint v) ;
		void AddElementFront(ControlPoint v) ;
		void SetColor(sf::Color c)		{ Col = c;					}
		void SetLength(float l)			{ Length = l;				}
		void SetWidth(int w)			{ Width = w;				}
		void AddLength(int l)			{ Points.back().Length += l;}
		int  GetLength(int el=-1);
		void UpdateLength();

		Snake(void) : Speed(80.0f), Moved(false), TimeStep(0), tailMove(true) {}
		~Snake(void) {}
	};

};
