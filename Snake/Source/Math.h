#pragma once
#include<math.h>

namespace MATH
{
	const float PI = 3.1415926f;

	struct Vec2
	{
		float x, y;

		Vec2() {}
		Vec2(float u, float v) : x(u), y(v) {}

		Vec2 operator+(const Vec2& v) const { return Vec2(v.x+x, v.y+y); }
		Vec2 operator-(const Vec2& v) const { return Vec2(x-v.x, y-v.y); }
		Vec2 operator*(const Vec2& v) const { return Vec2(x*v.x, y*v.y); }
		Vec2 operator*(float f) const { return Vec2(x*f, y*f); }
		Vec2 operator/(float f) const { return Vec2(x/f, y/f); }

		Vec2& operator+=(const Vec2& v) { x+=v.x; y+=v.y; return *this; }
		Vec2& operator-=(const Vec2& v) { x-=v.x; y-=v.y; return *this; }
		Vec2& operator*=(const Vec2& v) { x*=v.x; y*=v.y; return *this; }
		Vec2& operator/=(const Vec2& v) { x/=v.x; y/=v.y; return *this; }

		bool operator==(const Vec2& v) const { return (v.x==x && v.y==y); }
		bool operator!=(const Vec2& v) const { return (v.x!=x || v.y!=y); }

		float Length() const { return sqrt(pow(x, 2) + pow(y, 2)); }
		Vec2 Normalize() const { float l = Length(); return Vec2(x/l, y/l); }


		Vec2 yx() { return Vec2(y, x); }
	};

	struct Rect
	{
		float x, y, w, h;

		Rect(float _x, float _y, float _w, float _h) : x(_x), y(_y), w(_w), h(_h) {}
		Rect() {}

		Rect operator+(const Vec2 v) const { return Rect(x+v.x, y+v.y, w, h); }

		Vec2 GetCenter() { return Vec2(x+w*0.5f, y+h*0.5f); }

		static bool Intersection(const Rect& r1, const Rect& r2, Rect& result);
	};

	namespace Test
	{
		bool RectPoint(const Rect& r, const Vec2& v);
		bool RectRect(const Rect& r, const Rect& r2, Vec2& pushVec);
	};
};
