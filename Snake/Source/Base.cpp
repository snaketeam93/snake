#include"Math.h"
#include<math.h>
#include<algorithm>

using namespace std;

bool MATH::Test::RectPoint(const Rect& r, const Vec2& v)
{
	return (v.x > r.x && v.x < r.x+r.w && v.y > r.y && v.y < r.y+r.h);
}

float sign(float v)
{
    return v / abs(v);
}

bool MATH::Test::RectRect(const Rect& r, const Rect& r2, Vec2& pushVector)
{
    float right1 = r.x+r.w;
    float bottom1 = r.y+r.h;
    float right2 = r2.x+r2.w;
    float bottom2 = r2.y+r2.h;

    float xAxis = max(right1, right2) - min(r.x, r2.x);
    float yAxis = max(bottom1, bottom2) - min(r.y, r2.y);

    if (xAxis < r.w+r2.w &&
        yAxis < r.h+r2.h)
    {
        float xOverlap = (r.w+r2.w - xAxis)*sign(r2.x-r.x);
        float yOverlap = (r.h+r2.h - yAxis)*sign(r2.y-r.y);

        if (abs(xOverlap - yOverlap) < 0.001) pushVector = Vec2(xOverlap, yOverlap);
        else
        {
            if (xOverlap > yOverlap) pushVector = Vec2(0.0, yOverlap);
            else pushVector = Vec2(xOverlap, 0.0);
        }

        return true;
    }

    return false;
}
